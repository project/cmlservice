<?php
// $Id $

function cmlservice_admin_settings() {
/*  
  $form['general']['cmlservice_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose'),
    '#default_value' =>  FALSE,
    '#description' => t('Display alias changes (except during bulk updates).'),
  );
  return system_settings_form($form);
  */
}

function cmlservice_form($form_state){
  global $system_path, $base_path;
  
  $abort = FALSE;
  $processing = array();
  $system_path = '.' . $base_path . variable_get('cml_data_path', drupal_get_path('module', 'cmlservice'));
  
  // Check
  if(!is_dir("$system_path")) 
    $processing[] = t('Can`t find the cml directory. ') . l(t('Please select it here'), 'admin/store/settings/cml');
  if(!is_file("$system_path/import.xml")) 
    $processing[] = t('There is no import file.');
  if(!is_file("$system_path/offers.xml"))
    $processing[] = t('There is no offers file.');
  if(!is_dir("$system_path/import_files")) 
    $processing[] = t('Can`t find the import_files directory.');    
  if(!function_exists('simplexml_load_file'))
    $processing[] = t('<b style=color:red>Emerge does not compile dev-lang/php with simplexml by default. To enable this option, add the flag simplexml to your USE variable and recompile php. Please go to "http://www.php.net/manual/en/simplexml.installation.php" for additional informations.</b>');
     
  if(count($processing)>0){     
    $processing[]='<b style=color:red>'.t('No more tasks to continue.').'</b>';   
  }else{
    $plugins = cmlservice_plugins();
    $flags = cmlservice_get_flags();
    
    foreach($plugins as $name => $plugin){
      $options = array();
      $options_set = array();
      
      if(is_array($plugin['options']))  
       foreach($plugin['options'] as $option => $description){
           $options[$option] = $description;
           if($flags[$option]) $options_set[$option] = $option;
       }
      else{
          $options[$name] = $plugin['options'];
      }
    
       $form[$name] = array(
          '#type' => 'checkboxes',
          '#title' => $plugin['title'],
          '#description' => $plugin['desription'],
          '#default_value' => $options_set ? $options_set : array(),
          '#options' => $options,
          //'#disabled' => TRUE,
      );
    }   
        
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }
  
  $form['processing'] = array(
    '#type' => 'item',
    '#title' => 'ver: ' . CML_VER,
    //'#value' => $processing ? implode("<br>", $processing) : '',
  );
  
  drupal_set_message(implode("<br>", $processing), 'error'); 
  
return $form;
}

function cmlservice_form_submit($form, &$form_state) {
  cmlservice_run($form_state['values']);
}

function cmlservice_page() {
  $output = drupal_get_form('cmlservice_form');
  return $output;
}

function cml_settings(){
  $output = drupal_get_form('cml_settings_form');
  return $output;
}

function cml_settings_form(&$form_state){
  $form['cml_data_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to folder'),
    '#description' => t('Path must be without /import_files'),
    '#default_value' =>  variable_get('cml_data_path', drupal_get_path('module', 'cmlservice') . '/import_files'),
  );
  $form['cml_product_altered_price'] = array(
    '#type' => 'checkbox',
    '#title' => t('Altered price on/off'),
    '#default_value' => variable_get('cml_product_altered_price', FALSE),
    '#description' => t(''),
  );  
  $form['Order user info'] = array(
     '#type' => 'fieldset',
     '#title' => t('Order user info'),
     '#collapsible' => TRUE,
     '#collapsed' => FALSE,
  );  
  $form['Order user info']['cml_order_user_info'] = array(
    '#title' => t('Order info'),
    '#type' => 'radios',
    '#default_value' => variable_get('cml_order_user_info', 0),
    '#options' => array(
      0 => t('Use billing data for order'), // Платежной информации
      1 => t('Use delivery data for order'), // Информации о доставки
    ),
  );
  
  $form['cml_product_format'] = filter_form(
    variable_get('cml_product_format', FILTER_FORMAT_DEFAULT) ,NULL , array('cml_product_format'));  
  $form['cml_product_format']['#collapsed'] = FALSE;
         
  $form = system_settings_form($form);
  
  return $form;
}